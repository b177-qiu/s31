// Route defines when a particular controller will be used

const express = require("express");

// Create a Router instance that functions as a routing system
const router = express.Router();

// Import the taskControllers
const taskController = require("../controllers/taskControllers");

// Route to get all the tasks

// endpoint: localhost:3001/tasks
router.get("/",(req,res)=>{
	taskController.getAllTasks().then(resultFromController=> res.send(resultFromController));
})

// endpoint: localhost:3001/tasks/getTask

/*router.get("/getTask",(req,res)=>{
	taskController.getAllTasks().then(resultFromController=> res.send(resultFromController));
})*/

// Route to create a task
router.post("/",(req,res)=>{
	taskController.createTask(req.body).then(resultFromController=>res.send(resultFromController));
})

// Route to delete a task
// endpoint: localhost:3001/tasks/1234342(id no.)
router.delete("/:id", (req,res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController=> res.send(resultFromController));
})

// Route to update a task
router.put("/:id", (req,res)=>{
	// request(req) -> coming from req.params.id
	// response(res) -> coming from res.send
	taskController.updateTask(req.params.id, req.body).then(resultFromController=>res.send(resultFromController));
})


// Create a route for getting a specific task
router.get("/:id",(req,res)=>{
	taskController.getTask(req.params.id).then(resultFromController=> res.send(resultFromController));
})

// Create a route for changing the status of a task to "complete"
router.put("/:id/complete", (req,res)=>{
	taskController.updateStatus(req.params.id, req.body).then(resultFromController=>res.send(resultFromController));
})

// export the router object to be used in index.js
module.exports = router;